# Slides

A simple example to create slides with Markdown.

* [Slides as HTML](https://axel-klinger.gitlab.io/slides/slides.html)

Start slides locally with [reveal-md](https://github.com/webpro/reveal-md)

```
npm install -g reveal-md
```
```
reveal-md slides.md -w --css style.css
```
